﻿#include "../../beans/headers/SquareItem.h"

// 小正方形的边长
qreal SquareItem::SQUAREITEM_WIDTH = 6;

SquareItem::SquareItem(int itemDirection,QGraphicsItem *parent):QGraphicsRectItem(parent)
{
    this->setData(ITEM_CLASS,EnumType::ItemClass_SquareItem);
    this->setData(ITEM_TYPE,itemDirection);
    this->setCursorWithFlag(itemDirection);
}

// 所有绘制都要被限制在这个矩形内部
QRectF SquareItem::boundingRect(void) const
{
    // 宽和直径设置为FLAGITEM_WIDTH
    return QRectF(0,0,SQUAREITEM_WIDTH,SQUAREITEM_WIDTH);
}

void SquareItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    Q_UNUSED(option)
    Q_UNUSED(widget)
    painter->setPen(QPen(QColor(Qt::black),1));
    painter->drawPath(this->shape());
}

// 碰撞检测的轮廓（被contains() 和 collidesWithPath()使用）
QPainterPath SquareItem::shape(void) const
{
    QPainterPath path;
    path.addRect(this->boundingRect());
    return path;
}

// 辅助函数（根据m_ItemDirection设置鼠标显示样式）
void SquareItem::setCursorWithFlag(int itemDirection)
{
    switch (itemDirection)
    {
    case EnumType::SquareItem_LeftTop:
        this->setCursor(Qt::SizeFDiagCursor);
        break;
    case EnumType::SquareItem_LeftBottom:
        this->setCursor(Qt::SizeBDiagCursor);
        break;
    case EnumType::SquareItem_RightBottom:
        this->setCursor(Qt::SizeFDiagCursor);
        break;
    case EnumType::SquareItem_RightTop:
        this->setCursor(Qt::SizeBDiagCursor);
        break;

    default:
        break;
    }
}
