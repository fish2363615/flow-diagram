﻿#ifndef BLOCKITEMCLASS_H
#define BLOCKITEMCLASS_H

// Qt
#include <QObject>
#include <QGraphicsDropShadowEffect>
#include <QGraphicsItem>
#include <QMap>
#include <QList>
#include <QVector>
#include <QCursor>
#include <QColor>

// Josn
#include <QJsonObject>
#include <QJsonArray>

// 自定义继承类
#include "../../views/headers/ItemScene.h"
#include "../../beans/headers/FlowBlockItem.h"
#include "../../beans/headers/DotItem.h"
#include "../../beans/headers/SquareItem.h"
#include "../../beans/headers/TextItem.h"

// 自定义枚举
#include "../../tools/headers/EnumHeader.h"

// 元素外形尺寸
// BlockItem的矩形边框线宽
#define BLOCK_WIDTH 100
#define BLOCK_HEIGHT 50

class BlockItemClass:QObject
{
    Q_OBJECT
public:
    BlockItemClass(int blockTypeID,int blockID,QObject *parent = Q_NULLPTR);
    ~BlockItemClass(void);

    // 辅助方法（初始化模块元素）
    void initial(void);

    // 释放BlockItem
    void deleteBlockItem(void);

    // 辅助方法（显示场景模块）
    void showItem(ItemScene *scene);

    // 更新显示：报错本身和子部件已经内容显示
    void updateBlockItem(void);

    // 辅助方法（缩放模块单元）
    void zoomBlockItem(SquareItem *squareItem,QPointF pos);

    // 取值方法（模块位置,存模块的左上和右下）
    QVector<qreal> getBlockPos(void);

    // 公有函数（设置小正方形SquareItem是否可以被选中）
    void setSquareItemIsSelectable(bool flag);

    // 公有函数（设置圆点DotItem的是否可以被选中）
    void setDotItemIsSelectable(bool flag);

    // 公有函数
    DotItem *getDotItemByDirection(int DotItem_Direction);

    // 将BlockItemClass整体转化为Json对象
    QJsonObject toJsonObj(void);

    FlowBlockItem *getBlockItem() const;

    TextItem *getTextItem() const;

    QList<DotItem *> getDotItem_List() const;

    QList<SquareItem *> getSquareItem_List() const;

    int getBlockID() const;

private slots:

    // 槽函数（BlockItem是否被选中）
    void slot_BlockItemIsSelected(bool isSelected = false);

    // 槽函数（鼠标在BlockItem上时显示圆点，不在就不显示）
    void slot_BlockItemContainsCursorPos(bool isContains);

private:

    // 辅助方法（创建空白模块单元）
    FlowBlockItem *createBlockItem(void);

    // 辅助方法（创建空白模块文本）
    TextItem *createTextItem(void);

    // 辅助方法（创建端口单元）
    QList<DotItem *> createDotItems(void);

    // 辅助方法（创建模块缩放单元）
    QList<SquareItem *> createSquareItems(void);

    // 辅助方法（更新模块文本, 更新位置和显示内容）
    void updateTextItem(void);

    // 辅助方法（更新缩放控件）
    void updateDotItem(void);

    // 辅助方法（更新缩放控件）
    void updateSquareItem(void);

    // 辅助函数（设置小正方形FlagItem的是否显示）
    void setSquareItemVisible(bool is_Visible = false);

    // 辅助函数（设置圆点FlagItem的是否显示）
    void setDotItemVisible(bool is_Visible = false);

    // 辅助函数（碰撞检测）
    bool collidingItems(void);

private:
    // 模块序号
    int m_BlockID;

    // 模块类型
    int m_BlockType;

    // 模块框图
    FlowBlockItem *m_BlockItem;

    // 模块中心文本
    TextItem *m_TextItem;

    // 端口列表
    QList<DotItem *> m_DotItem_List;

    // 缩放小正方形列表
    QList<SquareItem *> m_SquareItem_List;
};

#endif // BLOCKITEMCLASS_H
