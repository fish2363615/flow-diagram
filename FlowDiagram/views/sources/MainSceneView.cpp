﻿#include "../headers/MainSceneView.h"

MainSceneView::MainSceneView(QWidget *parent)
{

}

ItemScene *MainSceneView::scene() const
{
    return m_ItemScene;

}

void MainSceneView::setScene(ItemScene *itemScene)
{
    QGraphicsView::setScene(itemScene);
    this->m_ItemScene = itemScene;
}
