﻿#include "../../views/headers/MainListView.h"

MainListView::MainListView(QWidget *parent):QListView(parent)
{
    initListView();
}

MainListView::~MainListView()
{

}

// 初始化函数（初始化listView）
void MainListView::initListView(void)
{
    m_Model = new QStandardItemModel;
    QStandardItem *item_1 = new QStandardItem(QIcon(":/images/images/pngs/圆角矩形_#333333_128_3633716.png"),"");
    item_1->setData(EnumType::BlockItemType_ArcRect,ITEM_TYPE_ID);

    QStandardItem *item_2 = new QStandardItem(QIcon(":/images/images/pngs/菱形_#333333_128_3638520.png"),"");
    item_2->setData(EnumType::BlockItemType_Diamond,ITEM_TYPE_ID);

    QStandardItem *item_3 = new QStandardItem(QIcon(":/images/images/pngs/圆角矩形_#333333_128_3633716.png"),"");
    item_3->setData(EnumType::BlockItemType_Rect,ITEM_TYPE_ID);

    QStandardItem *item_4 = new QStandardItem(QIcon(":/images/images/pngs/弧边矩形_#333333_128_3634581.png"),"");
    item_4->setData(EnumType::BlockItemType_ArcRect,ITEM_TYPE_ID);

    QStandardItem *item_5 = new QStandardItem(QIcon(":/images/images/pngs/菱形_#333333_128_3638520.png"),"");
    item_5->setData(EnumType::BlockItemType_Diamond,ITEM_TYPE_ID);

    QStandardItem *item_6 = new QStandardItem(QIcon(":/images/images/pngs/圆角矩形_#333333_128_3633716.png"),"");
    item_6->setData(EnumType::BlockItemType_Rect,ITEM_TYPE_ID);

    QStandardItem *item_7 = new QStandardItem(QIcon(":/images/images/pngs/弧边矩形_#333333_128_3634581.png"),"");
    item_7->setData(EnumType::BlockItemType_ArcRect,ITEM_TYPE_ID);

    QStandardItem *item_8 = new QStandardItem(QIcon(":/images/images/pngs/菱形_#333333_128_3638520.png"),"");
    item_8->setData(EnumType::BlockItemType_Diamond,ITEM_TYPE_ID);

    QStandardItem *item_9 = new QStandardItem(QIcon(":/images/images/pngs/圆角矩形_#333333_128_3633716.png"),"");
    item_9->setData(EnumType::BlockItemType_Rect,ITEM_TYPE_ID);

    m_Model->appendRow(item_1);
    m_Model->appendRow(item_2);
    m_Model->appendRow(item_3);
    m_Model->appendRow(item_4);
    m_Model->appendRow(item_5);
    m_Model->appendRow(item_6);
    m_Model->appendRow(item_7);
    m_Model->appendRow(item_8);
    m_Model->appendRow(item_9);

    this->setModel(m_Model);

    m_Drag = Q_NULLPTR;
    m_MimeData = Q_NULLPTR;
}

QStandardItemModel *MainListView::getStandardItemModel()
{
    return this->m_Model;
}

//  鼠标按压事件
void MainListView::mousePressEvent(QMouseEvent *event)
{
    QListView::mousePressEvent(event);

    if(event->button() == Qt::LeftButton)
    {

        this->setCursor(Qt::DragMoveCursor);

        int itemTypeID =this->m_Model->itemFromIndex(currentIndex())->data(ITEM_TYPE_ID).toInt();

        m_Drag = new QDrag(this);
        m_MimeData = new MimeData;

        m_MimeData->setMime("ItemTypeID",itemTypeID);
        m_Drag->setMimeData(m_MimeData);
        m_Drag->exec(Qt::MoveAction);
    }
}

// 鼠标释放事件
void MainListView::mouseReleaseEvent(QMouseEvent *event)
{
    QListView::mouseReleaseEvent(event);

    this->setCursor(Qt::ArrowCursor);
}

// 鼠标移动事件
void MainListView::mouseMoveEvent(QMouseEvent *event)
{
    QListView::mouseMoveEvent(event);
    this->setCursor(Qt::ArrowCursor);
}

