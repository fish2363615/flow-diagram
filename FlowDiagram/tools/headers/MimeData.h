﻿#ifndef MIMEDATA_H
#define MIMEDATA_H

#include <QMimeData>
#include <QStringList>
#include "../../tools/headers/EnumHeader.h"


class MimeData : public QMimeData
{
    Q_OBJECT
public:


    MimeData();

    // 存储MIME类型的数据
    void setMime(QString flag,int itemTypeID_Data);

    bool hasFormat(const QString &mimetype) const;

    int getData(QString mimetype) const;
    int getData(void) const;

    // 返回自定义的MIME类型列表
    QStringList formats() const;

protected:
    QVariant retrieveData(const QString &mimetype,QVariant::Type preferredType) const;

private:
    // MimeData的键值
    QStringList m_StringList;

    // Item的类别ID
    int m_ItemTypeID;

};

#endif // MIMEDATA_H
