﻿#ifndef DOTITEM_H
#define DOTITEM_H

#include <QObject>
#include <QGraphicsEllipseItem>
#include <QGraphicsSceneMouseEvent>
#include <QPainter>
#include <QCursor>

#include "../../tools/headers/EnumHeader.h"

class DotItem :public QObject, public QGraphicsEllipseItem
{
    Q_OBJECT
public:
    // 小正方形的边长
    static qreal DOTITEM_WIDTH;
    static qreal DOTITEMVIEW_WIDTH;

public:
    DotItem(int itemDirection,QGraphicsItem *parent = Q_NULLPTR);

    // 所有绘制都要被限制在这个矩形内部
    QRectF boundingRect(void) const;

    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget = Q_NULLPTR);

    // 碰撞检测的轮廓（被contains() 和 collidesWithPath()使用）
    QPainterPath shape(void) const;


};

#endif // DOTITEM_H
