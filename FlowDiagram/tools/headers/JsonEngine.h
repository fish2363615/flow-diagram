﻿#ifndef JSONENGINE_H
#define JSONENGINE_H

#include <QFile>

#include <QJsonDocument>
#include <QJsonParseError>
#include <QJsonObject>
#include <QJsonArray>

#include <QDebug>

class JsonEngine
{
public:
    JsonEngine(QString filePath);

    // f辅助函数（解析JSON数据）
    void analysisJsonData(void);

    //
    QJsonDocument toJsonDocument(void);
private:

    // 辅助函数（通过文件名将文件内容全部读取到内存中）
    QByteArray readFile(QString file_Path);
private:

    QByteArray m_FileAllData;

};

#endif // JSONENGINE_H
