﻿#ifndef MAINSCENEVIEW_H
#define MAINSCENEVIEW_H

#include <QObject>
#include <QGraphicsView>
#include "ItemScene.h"

class MainSceneView : public QGraphicsView
{
    Q_OBJECT
public:
    MainSceneView(QWidget *parent = Q_NULLPTR);
    ItemScene *scene() const;
    void setScene(ItemScene *itemScene);


private:
    ItemScene *m_ItemScene;
};

#endif // MAINSCENEVIEW_H
