﻿#include "../../tools/headers/MimeData.h"

MimeData::MimeData()
{

}

// 存储MIME类型的数据
void MimeData::setMime(QString flag,int itemTypeID_Data)
{
    if(!hasFormat(flag))
    {
        m_StringList.append(flag);
    }

    m_ItemTypeID = itemTypeID_Data;
}

bool MimeData::hasFormat(const QString &mimetype) const
{
    if(m_StringList.contains(mimetype))
        return true;
    else
        return QMimeData::hasFormat(mimetype);
}

int MimeData::getData(QString mimetype) const
{
    if(hasFormat(mimetype))
        return this->m_ItemTypeID;
    else
        return 0;
}

int MimeData::getData(void) const
{
    return this->m_ItemTypeID;
}

// 返回自定义的MIME类型列表
QStringList MimeData::formats() const
{
    return m_StringList;
}

QVariant MimeData::retrieveData(const QString &mimetype,QVariant::Type preferredType) const
{
    return QMimeData::retrieveData(mimetype,preferredType);
}
