﻿#include "../../beans/headers/DotItem.h"

// 小正方形的碰撞检测边长
qreal DotItem::DOTITEM_WIDTH = 12;

// 小正方形的视觉边长
qreal DotItem::DOTITEMVIEW_WIDTH = 6;


DotItem::DotItem(int itemDirection,QGraphicsItem *parent):QGraphicsEllipseItem(parent)
{
    this->setData(ITEM_CLASS,EnumType::ItemClass_DotItem);
    this->setData(ITEM_TYPE,itemDirection);
    this->setCursor(Qt::CrossCursor);
}

// 所有绘制都要被限制在这个矩形内部
QRectF DotItem::boundingRect(void) const
{
    // 宽和直径设置为DOTITEM_WIDTH
    QRectF rect = QRectF(0,0,DOTITEM_WIDTH,DOTITEM_WIDTH);
    return rect;
}

void DotItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    Q_UNUSED(option)
    Q_UNUSED(widget)

    qreal posX = (DOTITEM_WIDTH - DOTITEMVIEW_WIDTH) / 2;
    qreal posY = (DOTITEM_WIDTH - DOTITEMVIEW_WIDTH) / 2;

    QRectF rectangle(posX, posY, DOTITEMVIEW_WIDTH, DOTITEMVIEW_WIDTH);

    // 设置原点的填充色
    painter->setBrush(QBrush(QColor(97,38,28)));
    painter->setPen(QPen(QColor(Qt::black),1));
    painter->drawEllipse(rectangle);

    painter->setBrush(QBrush(QColor(3,21,127,50)));
    painter->drawPath(shape());
}

// 碰撞检测的轮廓（被contains() 和 collidesWithPath()使用）
QPainterPath DotItem::shape(void) const
{
    QPainterPath path;
    path.addEllipse(this->boundingRect());
    return path;
}
