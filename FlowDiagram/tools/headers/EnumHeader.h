﻿#ifndef ENUMHEADER_H
#define ENUMHEADER_H

#include <QObject>
#include <QMetaEnum>

// Item的Class
#define ITEM_CLASS  Qt::UserRole+1

// Item的相同Class中的不同分别
#define ITEM_TYPE  Qt::UserRole+2

class EnumType : public QObject
{
    Q_OBJECT
public:
    // BlockItem的类别
    enum BlockItemType
    {
        // 弧边矩形
        BlockItemType_ArcRect = 1,

        // 菱形
        BlockItemType_Diamond,

        // 矩形
        BlockItemType_Rect,

        // 平行四边形
        BlockItemType_Parallelogram = 4
    };
    Q_ENUM(BlockItemType)

    // BlockItem的SquareItem的所在方位
    enum SquareItemDirection
    {
        SquareItem_LeftTop = 5,
        SquareItem_LeftBottom,
        SquareItem_RightBottom,
        SquareItem_RightTop = 8
    };
    Q_ENUM(SquareItemDirection)

    // BlockItem的DotItem的所在方位
    enum DotItemDirection
    {
        DotItem_Left = 9,
        DotItem_Bottom,
        DotItem_Right,
        DotItem_Top = 12
    };
    Q_ENUM(DotItemDirection)

    enum LineItemDirection
    {
        LineItem_Left = 13,
        LineItem_Down,
        LineItem_Right,
        LineItem_Up = 16
    };
    Q_ENUM(LineItemDirection)

    // item的Class类
    enum ItemClass
    {
        // UserType = 65536
        ItemClass_FLowBlockItem = 65536 + 1,
        ItemClass_FLowLineItem,
        ItemClass_SquareItem,
        ItemClass_DotItem,
        ItemClass_TextItem,
        ItemClass_QGraphicsPolygonItem
    };
    Q_ENUM(ItemClass)
};

#endif // ENUMHEADER_H
