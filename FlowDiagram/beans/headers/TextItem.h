﻿#ifndef TEXTITEM_H
#define TEXTITEM_H

#include <QGraphicsTextItem>
#include <QFocusEvent>
#include <QGraphicsSceneMouseEvent>
#include <QKeyEvent>
#include <QCursor>

#include "../../tools/headers/EnumHeader.h"

class TextItem : public QGraphicsTextItem
{
    Q_OBJECT
public:
    TextItem(QGraphicsItem *parent = Q_NULLPTR);

    QRectF boundingRect(void) const;
    QPainterPath shape(void) const;


    qreal Width() const;
    void setWidth(const qreal &Width);

    qreal Height() const;
    void setHeight(const qreal &Height);

protected:
    // 获取焦点事件
    void focusInEvent(QFocusEvent *event) ;

    // 失去焦点事件
    void focusOutEvent(QFocusEvent *event) ;

    // 按键事件（点击回车失去焦点）
    void keyPressEvent(QKeyEvent *event) ;

signals:
    void signal_Edited();

private:
    qreal m_Width;
    qreal m_Height;
};

#endif // TEXTITEM_H
