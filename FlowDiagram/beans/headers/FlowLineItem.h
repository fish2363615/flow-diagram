﻿#ifndef FLOWLINEITEM_H
#define FLOWLINEITEM_H

#include <QGraphicsLineItem>
#include <QtWidgets> // 用于QStyle::State_Selected
#include <QPainter> // 用于pen() 和 line()

#include "../../tools/headers/EnumHeader.h"

class FlowLineItem : public QObject, public QGraphicsLineItem
{
    Q_OBJECT
public:
    FlowLineItem(QGraphicsItem *parent = Q_NULLPTR) ;

    // 所有绘制都要被限制在这个矩形内部
    QRectF boundingRect(void) const;

    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget = Q_NULLPTR);

    // 碰撞检测的轮廓（被contains() 和 collidesWithPath()使用）
    QPainterPath shape(void) const;

    // 辅助函数（碰撞检测，只用父级别和父级别有重叠部分才会返回true）
    bool collidingItems(void);

    // 外部函数（判断两条线是否重合）
    bool isSuperposition(FlowLineItem *lineItem);

    // 外部函数（判断线时水平还是竖直：true为竖直，false为水平）
    bool isVH();

    // 外部函数（获取线的方向，只适用于正交线）
    int lineDirection();

    // 判断其是否是点线（首尾点重合的线）
    bool isPointLine(void);


};

#endif // FLOWLINEITEM_H
