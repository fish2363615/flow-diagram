﻿#ifndef FLOWBLOCKITEM_H
#define FLOWBLOCKITEM_H

#include <QGraphicsItem>
#include <QGraphicsTextItem>
#include <QtWidgets>

#include "../../tools/headers/EnumHeader.h"
#include "../../beans/headers/TextItem.h"
#include "../../beans/headers/SquareItem.h"
#include "../../beans/headers/DotItem.h"

class FlowBlockItem : public QObject, public QGraphicsItem
{
    Q_OBJECT

    // Q_INTERFACES 告诉Qt类实现了哪些接口
    Q_INTERFACES(QGraphicsItem)

public:
    explicit FlowBlockItem(int itemTypeID,int flowBlockItemID = 1,QGraphicsItem *parent = Q_NULLPTR);
    ~FlowBlockItem(void);

    // 所有绘制都要被限制在这个矩形内部
    QRectF boundingRect(void) const;

    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget = Q_NULLPTR);

    // 碰撞检测的轮廓（被contains() 和 collidesWithPath()使用）
    QPainterPath shape(void) const;

    void setWidth(const qreal &Width);

    void setHeight(const qreal &Height);

    int FlowBlockItemID() const;
    void setFlowBlockItemID(int FlowBlockItemID);

    int ItemTypeID() const;
    void setItemTypeID(int ItemTypeID);

protected:
    void mouseDoubleClickEvent(QGraphicsSceneMouseEvent *event);

signals:
    // BlockItem被双击被选中后，需要端口和缩放单元都要显示出来
    void signal_BlockItemIsSelected(bool isSelected);

    // BlockItem被双击，中心文本需要获取焦点
    void signal_BlockItemDoubleClicked(void);

    // 鼠标在BlockItem上（也可以没有选中）
    void signal_BlockItemContainsSceneCursorPos(bool isContains);

private:
    // 辅助函数（初始化设置）
    void initial(void);

    // 辅助函数（碰撞检测）
    bool thisCollidingItems(void);

    // 辅助函数（场景中的光标坐标是否在此BlockItem上）
    bool containSceneCursorPos(QPointF cursotPos);


private:

    // 在QgraphicsScene中的唯一ID
    int m_FlowBlockItemID;

    // 该BlockItem类别ID（相对于图例中而言）
    int m_ItemTypeID;

    qreal m_Width;
    qreal m_Height;

};

#endif // FLOWBLOCKITEM_H
