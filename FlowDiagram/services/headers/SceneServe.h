﻿#ifndef SCENESERVE_H
#define SCENESERVE_H

#include <QObject>
#include "../../views/headers/ItemScene.h"
#include "../../tools/headers/EnumHeader.h"
#include "../../tools/headers/JsonEngine.h"

class SceneServe:public QObject
{
    Q_OBJECT
public:
    SceneServe(ItemScene *itemScene);
    ~SceneServe(void);

    // 公有函数（将JsonFile文件读入到ItemScene中）
    void readJsonFile2ItemScene(QString filePath);

private:
    // 辅助函数（将JsonObj解析成BlockItemClass）
    BlockItemClass *jsonObj2BlockItemClass(QJsonObject jsonObj);

    // 辅助函数（将JsonObj解析成LineItemClass）
    LineItemClass *jsonObj2LineItemClass(QJsonObject jsonObj);

    // 辅助函数（将BlockItemCalss添加到ItemScene中）
    void setBlockItemIntoItemScene(BlockItemClass *blockItemClass,ItemScene *itemScene);

    // 辅助函数（将lineItemCalss添加到ItemScene中）
    void setLineItemIntoItemScene(LineItemClass *lineItemClass,ItemScene *itemScene);

private:
    ItemScene *m_ItemScene;
};

#endif // SCENESERVE_H
