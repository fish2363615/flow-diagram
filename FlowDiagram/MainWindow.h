﻿#ifndef MAINWINDOW_H
#define MAINWINDOW_H
#pragma execution_character_set("utf-8")
#include <QMainWindow>
#include <QLabel>
#include <QFileDialog>
#include <QDebug>

#include "ItemsForm.h"
#include "FlowViewWindow.h"
#include "views/headers/ItemScene.h"


QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class SceneServe;
class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();


private slots:
    void slot_ActionOpenItemView(void);
    void slot_ActionOpenFlowView(void);

    // 槽函数（打开文件）
    void slot_ActionOpenFile(void);

    // 槽函数(保存文件)
    void slot_ActionSaveFile(void);

    void slot_CurrCursorPos(QPointF cursorPos);
    void slot_CurrCursorPosItemClass(QString itemClass);

private:

    // 初始化函数（类相设置关初始化）
    void init(void);

    void initToolBar(void);

    ItemScene *createItemScene(void);

private:
    Ui::MainWindow *ui;

    ItemScene *m_ItemScene;

    QLabel *m_CursorPosLabel,*m_CursorPosItemClassLabel;

};
#endif // MAINWINDOW_H
