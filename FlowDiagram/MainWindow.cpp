﻿#include "MainWindow.h"
#include "ui_MainWindow.h"

#include "views/headers/ItemScene.h"
#include "tools/headers/JsonEngine.h"
#include "services/headers/SceneServe.h"

#include <QDir>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    init();
}

MainWindow::~MainWindow()
{
    delete ui;
}

// 初始化函数（类相设置关初始化）
void MainWindow::init(void)
{
    this->setAttribute(Qt::WA_QuitOnClose,true);

    // 使得源文件和读取输出都采用UTF-8
    QTextCodec::setCodecForLocale(QTextCodec::codecForName("UTF-8"));

    // 状态栏显示鼠标坐标
    this->m_CursorPosLabel = new QLabel;
    this->m_CursorPosLabel->setText("(X:0.0,Y:0.0)");
    ui->statusbar->addWidget(this->m_CursorPosLabel);

    // 状态栏显示鼠标出的Item类型
    this->m_CursorPosItemClassLabel = new QLabel;
    this->m_CursorPosItemClassLabel->setText("ItemClass:NULL");
    ui->statusbar->addWidget(this->m_CursorPosItemClassLabel);

    // 在ToolBar中添加字体选框
    QFontComboBox *fontBox = new QFontComboBox;
    fontBox->setSizeIncrement(80,20);
    ui->toolBar->addWidget(fontBox);

    this->m_ItemScene = createItemScene();
    ui->graphicsView->setScene(m_ItemScene);

    // 设置该场景接受拖拽事件（因为是子部件，事件穿透到场景中）
    ui->graphicsView->setAcceptDrops(true);

}

ItemScene *MainWindow::createItemScene(void)
{
    ItemScene *itemScene;

    itemScene = new ItemScene;
    itemScene->setSceneRect(0,0,1000,800);

    // 获取场景中的鼠标坐标
    connect(itemScene,SIGNAL(signal_CurrCursorPos(QPointF)),this,SLOT(slot_CurrCursorPos(QPointF)));

    // 获取场景中的鼠标坐标处的ItemClass类别
    connect(itemScene,SIGNAL(signal_CurrCursorPosItemClass(QString)),this,SLOT(slot_CurrCursorPosItemClass(QString)));

    return itemScene;
}

void MainWindow::slot_ActionOpenItemView(void)
{
    ItemsForm *itremForm = ItemsForm::getInstance();

    // 让窗口显示在最前面，但是并不一霸道一直在最前面
    itremForm->activateWindow();
    itremForm->setWindowState((itremForm->windowState() & ~Qt::WindowMinimized) | Qt::WindowActive);
    itremForm->show();

}


void MainWindow::slot_ActionOpenFlowView(void)
{
    FlowViewWindow *flowViewWindow = FlowViewWindow::getInstance();

    // 让窗口显示在最前面，但是并不一霸道一直在最前面
    flowViewWindow->activateWindow();
    flowViewWindow->setWindowState((flowViewWindow->windowState() & ~Qt::WindowMinimized) | Qt::WindowActive);
    flowViewWindow->show();
}

// 槽函数（打开文件）
void MainWindow::slot_ActionOpenFile(void)
{

    QString fileName;

    fileName = QFileDialog::getOpenFileName(this,"打开文件","","流程图(*.json)");

    this->m_ItemScene->clear();

    SceneServe sceneServe(this->m_ItemScene);

    sceneServe.readJsonFile2ItemScene(fileName);

}

// 槽函数(保存文件)
void MainWindow::slot_ActionSaveFile(void)
{
    QString fileName = QFileDialog::getSaveFileName(this,"设置保存路径","xxx.json","流程图(*.json)");
    QJsonDocument jsonDoc = m_ItemScene->toJsonDoc();
    QByteArray data = jsonDoc.toJson();

    QFile file(fileName);

    bool ok=file.open(QIODevice::WriteOnly);
    if(ok)
    {
        file.write(data);
        file.close();
    }
    else
    {
        qDebug()<<"write error!"<<endl;
    }
}

void MainWindow::slot_CurrCursorPos(QPointF cursorPos)
{
    QString str = QString("(X:%1,Y:%2)").arg(cursorPos.rx()).arg(cursorPos.ry());

    this->m_CursorPosLabel->setText(str);
}

void MainWindow::slot_CurrCursorPosItemClass(QString itemClass)
{
    QString str = QString("ItemClass:%1").arg(itemClass);
    this->m_CursorPosItemClassLabel->setText(str);
}

void MainWindow::initToolBar(void)
{

}




