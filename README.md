# FlowDiagram

#### FlowDiagram介绍

	简单实现以下功能：
		#1.在两个窗口之间拖拽传递信息
		#2.曼哈顿路线画线
		#3.曼哈顿路线优化
		#4.曼哈顿路线避障
		#5.模块的的四个角缩放
		#6.模块移动，连线跟随
		#7.信息保存为本地json格式文件
	演示视频：https://pan.baidu.com/s/1SZtPVagR9fYHr7ofTVJUXA  
	提取码：u1k9
	
	愿景：开发一个流程图软件，免费公开使用，包括简单地SYSML建模仿真功能




#### FlowDiagram实现技术

1.	使用子类化QMimeData类，需要传递的信息保存。（QMimeData起到载体作用）
2.	在拖出窗口中（这里是MainListView，因为Qt有事件穿透的特性）定义鼠标按压事件（mousePressEvent(QMouseEvent *event)），当鼠标点击MainListView中的item后，就新建QDrag对象和QMimeData的子类MimeData，将信息保存进MimeData中，再将MimeData保存进QDrag中。
3.	再在拖入的窗口中（这里是QGraphicsScene的ItemScene，因为Qt事件穿透特性,穿透顺序：FlowViewWindow->QGraohicsView->ItemScene）的ItemScene定义三个两个事件：void dragMoveEvent(QGraphicsSceneDragDropEvent *event) 和 void dropEvent(QGraphicsSceneDragDropEvent *event)。因为在QGraphicsScene中的dragMoveEvent事件将accept忽略了，所以要特地重写一下。

##慢哈顿正交路径算实现原理

1. 归一化思路：因为流程图式的正交链接线，最多只有5条线。因此将所有情况都归一化到5根线上的处理；
2. 确定首尾两端的
