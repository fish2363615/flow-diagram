﻿#include "../headers/SceneServe.h"
#include "../../beans/headers/BlockItemClass.h"

SceneServe::SceneServe(ItemScene *itemScene):m_ItemScene(itemScene)
{

}

SceneServe::~SceneServe(void)
{

}



// 公有函数（将JsonFile文件读入到ItemScene中）
void SceneServe::readJsonFile2ItemScene(QString filePath)
{
    JsonEngine jsonEngine(filePath);

    QJsonDocument jsonDoc = jsonEngine.toJsonDocument();

    QJsonObject rootObj = jsonDoc.object();

    QJsonArray blockItems = rootObj["BlockItems"].toArray();
    QJsonArray lineItems = rootObj["LineItems"].toArray();

    for(int i = 0; i < blockItems.size() - 1; ++i)
    {
        QJsonObject jsonObj = blockItems.at(i).toObject();
        BlockItemClass *blockItemClass = jsonObj2BlockItemClass(jsonObj);
        setBlockItemIntoItemScene(blockItemClass,this->m_ItemScene);
        m_ItemScene->setBlockItemClassInsertIntoBlockMap(blockItemClass->getBlockID(),blockItemClass);
    }

    for (int i = 0; i < lineItems.size() - 1; ++i)
    {

        QJsonObject jsonObj = lineItems.at(i).toObject();
        LineItemClass *lineItemClass = jsonObj2LineItemClass(jsonObj);
        setLineItemIntoItemScene(lineItemClass,this->m_ItemScene);
        m_ItemScene->setLineItemClassInsertIntoLineList(lineItemClass);
    }

}

// 将JsonObj解析成BlockItemClass
BlockItemClass *SceneServe::jsonObj2BlockItemClass(QJsonObject jsonObj)
{
    BlockItemClass *blockItemClass;
    FlowBlockItem *blockItem ;
    TextItem *textItem;

    int blockItem_ID,blockItem_TypeID;
    qreal blockItem_Width,blockItem_Height;
    QJsonArray blockPos;
    QPointF blockItem_Pos;

    QJsonObject textItem_JsonObj;
    QJsonArray fontColorArray;
    int fontColor_Red,fontColor_Green,fontColor_Blue,fontColor_Alpha;
    QColor textItem_Color;
    int textItem_Alignment;
    QString textItem_Text,textItem_Font;

    blockItem_ID = jsonObj["BlockItem_ID"].toInt();
    blockItem_TypeID = jsonObj["BlockItem_TypeID"].toInt();
    blockItem_Width = jsonObj["BlockItem_Width"].toDouble();
    blockItem_Height = jsonObj["BlockItem_Height"].toDouble();
    blockPos = jsonObj["BlockItem_Pos"].toArray();
    blockItem_Pos.setX(blockPos.at(0).toDouble());
    blockItem_Pos.setY(blockPos.at(1).toDouble());

    blockItemClass = new BlockItemClass(blockItem_TypeID,blockItem_ID);

    blockItem = blockItemClass->getBlockItem();
    blockItem->setWidth(blockItem_Width);
    blockItem->setWidth(blockItem_Height);
    blockItem->setPos(blockItem_Pos);

    textItem_JsonObj = jsonObj["BlockItem_TextItem"].toObject();

    textItem_Alignment = textItem_JsonObj["Alignment"].toInt();
    textItem_Font = textItem_JsonObj["Font"].toString();
    textItem_Text = textItem_JsonObj["Text"].toString();

    fontColorArray = textItem_JsonObj["FontColor"].toArray();
    fontColor_Red = fontColorArray.at(0).toInt();
    fontColor_Green = fontColorArray.at(1).toInt();
    fontColor_Blue = fontColorArray.at(2).toInt();
    fontColor_Alpha = fontColorArray.at(3).toInt();
    textItem_Color = QColor(fontColor_Red,fontColor_Green,fontColor_Blue,fontColor_Alpha);

    textItem = blockItemClass->getTextItem();
    textItem->setPlainText(textItem_Text);

    // QFont("SimSun,9,-1,5,50,0,0,0,0,0")
    QStringList stringFont = textItem_Font.split(',');
    QFont textFont;
    textFont.setFamily(stringFont.at(0));
    textFont.setPointSizeF(QString(stringFont.at(1)).toDouble()); // qreal
    textFont.setPixelSize(1); // int
    textFont.setStyleHint(QFont::Helvetica); // int
    textFont.setWeight(QString(stringFont.at(4)).toInt()); // int
    textFont.setStyle(QFont::StyleNormal); // int
    textFont.setUnderline(QString(stringFont.at(6)).toInt()); // bool
    textFont.setStrikeOut(QString(stringFont.at(7)).toInt()); // bool
    textFont.setFixedPitch(QString(stringFont.at(8)).toInt()); // bool

    textItem->setFont(textFont);
    textItem->setDefaultTextColor(textItem_Color);

    blockItemClass->updateBlockItem();

    return blockItemClass;
}

// 将JsonObj解析成LineItemClass
LineItemClass *SceneServe::jsonObj2LineItemClass(QJsonObject jsonObj)
{
    LineItemClass *lineItemClass;

    DotItem *souDotItem,*desDotItem;

    int lineItem_ID;
    int lineItem_SouBlockItemID,lineItem_DesBlockItemID;
    int lineItem_SouDotItemDirection,lineItem_DesDotItemDirection;
    QJsonArray lineItem_Pos;

    QList<QPointF> linesPos;

    lineItem_ID = jsonObj["LineItem_ID"].toInt();
    lineItem_SouBlockItemID = jsonObj["LineItem_SouBlockItemID"].toInt();
    lineItem_DesBlockItemID = jsonObj["LineItem_DesBlockItemID"].toInt();
    lineItem_SouDotItemDirection = jsonObj["LineItem_SouDotItemDirection"].toInt();
    lineItem_DesDotItemDirection = jsonObj["LineItem_DesDotItemDirection"].toInt();

    souDotItem = this->m_ItemScene->getBlockItemClass_Map().value(lineItem_SouBlockItemID)->getDotItemByDirection(lineItem_SouDotItemDirection);
    desDotItem = this->m_ItemScene->getBlockItemClass_Map().value(lineItem_SouBlockItemID)->getDotItemByDirection(lineItem_DesDotItemDirection);

    lineItem_Pos = jsonObj["LineItem_Pos"].toArray();

    for (int i = 0; i < lineItem_Pos.size(); i += 2)
    {
        QPointF pos(lineItem_Pos.at(i).toDouble(),lineItem_Pos.at(i + 1).toDouble());
        linesPos.append(pos);
    }

    lineItemClass = new LineItemClass(souDotItem,desDotItem,lineItem_ID,linesPos);

    return lineItemClass;
}

// 辅助函数（将BlockItemCalss添加到ItemScene中）
void SceneServe::setBlockItemIntoItemScene(BlockItemClass *blockItemClass,ItemScene *itemScene)
{
    itemScene->addItem(blockItemClass->getBlockItem());
    // blockItemClass->getBlockItem()->show();
}

// 辅助函数（将lineItemCalss添加到ItemScene中）
void SceneServe::setLineItemIntoItemScene(LineItemClass *lineItemClass,ItemScene *itemScene)
{
    QList<FlowLineItem *> lineList = lineItemClass->LineItems_List();

    foreach (FlowLineItem *lineItem, lineList)
    {
        itemScene->addItem(lineItem);
        // lineItem->show();
    }
}



