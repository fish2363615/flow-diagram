QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

HEADERS += \
    beans/headers/BlockItemClass.h \
    beans/headers/DotItem.h \
    beans/headers/FlowBlockItem.h \
    beans/headers/FlowLineItem.h \
    beans/headers/LineItemClass.h \
    beans/headers/SquareItem.h \
    beans/headers/TextItem.h \
    services/headers/SceneServe.h \
    tools/headers/BlockFactory.h \
    tools/headers/EnumHeader.h \
    tools/headers/JsonEngine.h \
    tools/headers/MimeData.h \
    views/headers/ItemScene.h \
    views/headers/MainListView.h \
    FlowViewWindow.h \
    ItemsForm.h \
    MainWindow.h \
    views/headers/MainSceneView.h

SOURCES +=  \
    FlowViewWindow.cpp \
    beans/sources/BlockItemClass.cpp \
    beans/sources/DotItem.cpp \
    beans/sources/FlowBlockItem.cpp \
    beans/sources/FlowLineItem.cpp \
    beans/sources/LineItemClass.cpp \
    beans/sources/SquareItem.cpp \
    beans/sources/TextItem.cpp \
    services/sources/SceneServe.cpp \
    tools/sources/BlockFactory.cpp \
    tools/sources/JsonEngine.cpp \
    tools/sources/MimeData.cpp \
    views/sources/ItemScene.cpp \
    views/sources/MainListView.cpp \
    ItemsForm.cpp \
    MainWindow.cpp\
    main.cpp \
    views/sources/MainSceneView.cpp

FORMS += \
    FlowViewWindow.ui \
    ItemsForm.ui \
    MainWindow.ui


# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    images.qrc
