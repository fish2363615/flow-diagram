﻿#ifndef FLOWVIEWWINDOW_H
#define FLOWVIEWWINDOW_H

#include <QMainWindow>
#include <QTextCodec>
#include <QPointF>
#include <QMouseEvent>

#include "views/headers/ItemScene.h"

namespace Ui {
class FlowViewWindow;
}

class FlowViewWindow : public QMainWindow
{
    Q_OBJECT

public:
    static FlowViewWindow *getInstance(QWidget *parent = nullptr);

    FlowViewWindow(QWidget *parent = nullptr);
    ~FlowViewWindow(void);
protected:
    void mouseMoveEvent(QMouseEvent *event);

private slots:
    // 槽函数（选择字体颜色）
    void slot_ActionSelectFontColor(void);

    // 槽函数（选择文本背景色）
    void slot_ActionSelectTextBackColor(void);

    // 槽函数（修改字体）
    void slot_ChangeTextItemFont(QString fontStr);

    // 槽函数（接收场景中的当前鼠标坐标）
    void slot_currCursorPos(QString strData);
private:
    void init(void);



private:
    Ui::FlowViewWindow *ui;
    static FlowViewWindow *m_FlowViewWindow;

    // 状态栏显示鼠标坐标
    QLabel *m_CursorPosLabel;

    ItemScene *m_ItemScene;

    QFontComboBox *m_FontComboBox;
};

#endif // FLOWVIEWWINDOW_H
