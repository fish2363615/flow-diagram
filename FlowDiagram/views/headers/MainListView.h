﻿#ifndef MAINLISTVIEW_H
#define MAINLISTVIEW_H
#pragma execution_character_set("utf-8")
#include <QListView>
#include <QMouseEvent>
#include <QStandardItemModel>
#include <QStandardItem>
#include <QMap>
#include <QDrag>
#include <QMetaEnum>

#include <QDebug>

#include "../../tools/headers/MimeData.h"
#include "../../tools/headers/EnumHeader.h"

// 将Qt::UserRole+1定义成ITEM_TYPE_ID，方便使用
#define ITEM_TYPE_ID Qt::UserRole+1
class ItemScene;
class MainListView : public QListView
{
    Q_OBJECT

public:
    MainListView(QWidget *parent = Q_NULLPTR);
    ~MainListView();
    QStandardItemModel *getStandardItemModel();
protected:
    void mousePressEvent(QMouseEvent *event);
    void mouseReleaseEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);

private:
    // 初始化函数（初始化listView）
    void initListView(void);

private:
    //
    QStandardItemModel *m_Model;

    QDrag *m_Drag ;
    MimeData *m_MimeData ;
};

#endif // MAINLISTVIEW_H
