﻿#include "MainWindow.h"

#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    // 加载国际化翻译文件
    QTranslator translator;

    // 需要将qt_zh_CN.qm放入到Debug或者Release的文件夹下
    translator.load(QDir::currentPath() + "/qt_zh_CN.qm");
    a.installTranslator(&translator);

    MainWindow w;
    w.show();
    return a.exec();
}
