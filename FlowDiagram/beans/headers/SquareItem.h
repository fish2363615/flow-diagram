﻿#ifndef SQUAREITEM_H
#define SQUAREITEM_H

#include <QObject>
#include <QGraphicsRectItem>
#include <QGraphicsSceneMouseEvent>
#include <QPainter>
#include <QCursor>
#include "../../tools/headers/EnumHeader.h"

class SquareItem :public QObject, public QGraphicsRectItem
{
    Q_OBJECT
public:
    // 小正方形的边长
    static qreal SQUAREITEM_WIDTH;

public:
    SquareItem(int itemDirection,QGraphicsItem *parent = Q_NULLPTR);

    // 所有绘制都要被限制在这个矩形内部
    QRectF boundingRect(void) const;

    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget = Q_NULLPTR);

    // 碰撞检测的轮廓（被contains() 和 collidesWithPath()使用）
    QPainterPath shape(void) const;

private:
    // 辅助函数（根据m_ItemDirection设置鼠标显示样式）
    void setCursorWithFlag(int itemDirection);
};

#endif // SQUAREITEM_H
