#ifndef ITEMSFORM_H
#define ITEMSFORM_H

#include <QWidget>
#include <QTextCodec>

namespace Ui {
class ItemsForm;
}

class ItemsForm : public QWidget
{
    Q_OBJECT

public:
    static ItemsForm *getInstance(QWidget *parent = Q_NULLPTR);

private:
    ItemsForm(QWidget *parent = Q_NULLPTR);
    ~ItemsForm(void);

    void init(void);


private:    
    Ui::ItemsForm *ui;
    static ItemsForm *m_ItemsForm;
};

#endif // ITEMSFORM_H
