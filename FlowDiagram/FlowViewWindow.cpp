﻿#include "FlowViewWindow.h"
#include "ui_FlowViewWindow.h"

FlowViewWindow::FlowViewWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::FlowViewWindow)
{
    ui->setupUi(this);
    init();
}

FlowViewWindow::~FlowViewWindow(void)
{
    // 单例模式时，需要特意加这一句
    m_FlowViewWindow = Q_NULLPTR;
}

// 辅助函数（初始化设置）
void FlowViewWindow::init(void)
{
    // 使得源文件和读取输出都采用UTF-8
    QTextCodec::setCodecForLocale(QTextCodec::codecForName("UTF-8"));

    // 配合主窗口中设置成true，实现关闭主窗口后整个程序退出。
    this->setAttribute(Qt::WA_QuitOnClose,false);

    // 设置管理此窗口后自动调用析构函数释放内存
    this->setAttribute(Qt::WA_DeleteOnClose);

    m_CursorPosLabel = new QLabel;
    m_CursorPosLabel->setText("(X:0.0,Y:0.0)");
    // this->ui->statusbar->addPermanentWidget(m_status1);//永久信息窗口 - 不会被一般消息覆盖
    this->ui->statusbar->addWidget(m_CursorPosLabel);
    // this->ui->statusbar->setSizeGripEnabled(false);//去掉状态栏右下角的三角

    // 新增一个场景
    m_ItemScene = new ItemScene;
    m_ItemScene->setSceneRect(0,0,1000,800);
    ui->flowView->setScene(m_ItemScene);

    // 设置该场景接受拖拽事件（因为是子部件，事件穿透到场景中）
    ui->flowView->setAcceptDrops(true);

    // 添加一个字体下拉选框
    m_FontComboBox = new QFontComboBox;
    ui->toolBar->addWidget(m_FontComboBox);
    connect(m_FontComboBox,SIGNAL(activated(QString)),this,SLOT(slot_ChangeTextItemFont(QString)));

    // 获取场景中的鼠标坐标
    connect(m_ItemScene,SIGNAL(signal_currCursorPos(QString)),this,SLOT(slot_currCursorPos(QString)));
}


// 单例模式
FlowViewWindow *FlowViewWindow::m_FlowViewWindow = Q_NULLPTR;
FlowViewWindow *FlowViewWindow::getInstance(QWidget *parent )
{
    if(m_FlowViewWindow == Q_NULLPTR)
        m_FlowViewWindow = new FlowViewWindow(parent);

    return m_FlowViewWindow;
}

void FlowViewWindow::mouseMoveEvent(QMouseEvent *event)
{

    QMainWindow::mouseMoveEvent(event);
}

// 槽函数（选择字体颜色）
void FlowViewWindow::slot_ActionSelectFontColor(void)
{
    /*
    QList<QGraphicsItem *> itemList =  this->m_ItemScene->selectedItems();

    if(itemList.count() > 0)
    {
        FlowBlockItem *flowBlockItem = qgraphicsitem_cast<FlowBlockItem *>(itemList.at(0));
        TextItem *textItem = flowBlockItem->getTextItem();

        QString str = QString::fromUtf8("选择颜色");

        // 处理电脑系统是GBK的格式，从UTF-8转化为GBK格式
        QByteArray byteStr = QTextCodec::codecForName("gbk")->fromUnicode(str);
        QString gbkStr = QTextCodec::codecForName("gbk")->toUnicode(byteStr);

        // 打开颜色选择器弹窗
        QColor fontColor = QColorDialog::getColor(QColor(Qt::white),this,gbkStr,QColorDialog::DontUseNativeDialog);

        // 设置字体颜色
        textItem->setDefaultTextColor(fontColor);
    }
    */
}

// 槽函数（选择文本背景色）
void FlowViewWindow::slot_ActionSelectTextBackColor(void)
{
    /*
    QList<QGraphicsItem *> itemList =  this->m_ItemScene->selectedItems();
    if(itemList.count() > 0)
    {
        FlowBlockItem *flowBlockItem = qgraphicsitem_cast<FlowBlockItem *>(itemList.at(0));
        TextItem *textItem = flowBlockItem->getTextItem();

    QString str = QString::fromUtf8("选择颜色");

    // 处理电脑系统是GBK的格式，从UTF-8转化为GBK格式
    QByteArray byteStr = QTextCodec::codecForName("gbk")->fromUnicode(str);
    QString gbkStr = QTextCodec::codecForName("gbk")->toUnicode(byteStr);

    // 打开颜色选择器弹窗
    QColor fontColor = QColorDialog::getColor(QColor(Qt::white),this,gbkStr,QColorDialog::DontUseNativeDialog);
}
*/
}

// 槽函数（修改字体）
void FlowViewWindow::slot_ChangeTextItemFont(QString fontStr)
{
    /*
   QList<QGraphicsItem *> itemList =  this->m_ItemScene->selectedItems();
   if(itemList.count() > 0)
   {
       FlowBlockItem *flowBlockItem = qgraphicsitem_cast<FlowBlockItem *>(itemList.at(0));
       TextItem *textItem = flowBlockItem->getTextItem();
       textItem->setFont(QFont(fontStr));

    // 由于字体改变可能会不居中，修正居中
    flowBlockItem->setTextItemCenter();
}
*/
}

// 槽函数（接收场景中的当前鼠标坐标）
void FlowViewWindow::slot_currCursorPos(QString strData)
{

    m_CursorPosLabel->setText(strData);
}

