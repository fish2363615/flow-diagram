﻿#include "ItemsForm.h"
#include "ui_ItemsForm.h"

#include <QDebug>

ItemsForm::ItemsForm(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ItemsForm)
{
    ui->setupUi(this);

    init();
}

ItemsForm::~ItemsForm(void)
{
    m_ItemsForm = Q_NULLPTR;
}

// 单例模式
ItemsForm *ItemsForm::m_ItemsForm = Q_NULLPTR;
ItemsForm *ItemsForm::getInstance(QWidget *parent)
{
    if(m_ItemsForm == Q_NULLPTR)
        m_ItemsForm = new ItemsForm(parent);

    return m_ItemsForm;
}


void ItemsForm::init(void)
{
    // 配合主窗口中设置成true，实现关闭主窗口后整个程序退出。
    this->setAttribute(Qt::WA_QuitOnClose,false);

    // 设置管理此窗口后自动调用析构函数释放内存
    this->setAttribute(Qt::WA_DeleteOnClose);

    // 使得源文件和读取输出都采用UTF-8
    QTextCodec::setCodecForLocale(QTextCodec::codecForName("UTF-8"));

}



