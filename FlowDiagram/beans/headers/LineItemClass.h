﻿#ifndef LINEITEMCLASS_H
#define LINEITEMCLASS_H

#include <QGraphicsPolygonItem>

#include <QJsonArray>
#include <QJsonObject>

#include "../../tools/headers/EnumHeader.h"
#include "../../beans/headers/FlowLineItem.h"
#include "../../beans/headers/DotItem.h"

// 第一个内联点的偏移量
#define OFFSET_SIZE 15.0

class ItemScene;
class FlowBlockItem;
class LineItemClass
{
public:
    LineItemClass(DotItem *souBlockItem,DotItem *dstBlockItem = Q_NULLPTR,int lineItemIDInScene = 0);
    LineItemClass(DotItem *souBlockItem,DotItem *dstBlockItem,int lineItemIDInScene,QList<QPointF> linePoints);

    ~LineItemClass(void);

    // 公有函数（画一条初始的曼哈顿路线）
    void drawManhattanLine(QPointF cursorPos);

    // 新建五条线
    void create5LineItems(void);

    // 公有函数（曼哈顿路径规划）
    void manhattanRoutePlan(void);

    // 辅助函数（实时归并线元组）
    void mergeRoute(void);

    // 辅助函数（该条线移动，与其连接的前后线的连接点也要更新，注意，线元组的起始和末尾点不会成为参数）
    void updateConnectiveLines(FlowLineItem *lineItem);

    // 清除5条线
    void clearLineItems(void);



    // 转换json对象
    QJsonObject toJsonObj(void);

    int LineItemID(void) const;
    void setLineItemID(int LineItemID);

    DotItem *SouDotItem(void) const;
    void setSouDotItem(DotItem *SouDotItem);

    DotItem *DesDotItem(void) const;
    void setDesDotItem(DotItem *DesDotItem);

    QList<FlowLineItem *> LineItems_List(void) const;


private:

    // 辅助函数（初始化）
    void initial(void);

    // 创建箭头三角形
    QGraphicsPolygonItem *createTriangleItem(void);

    // 辅助函数（获取每个LIneItem的碰撞的父级别BlockItem的列表）
    QList<FlowBlockItem *> collidingItems(FlowLineItem *lineItem);

    // 辅助函数（线元组优化：避障算法）
    void evadeBlcokItem(void);

    // 辅助函数（根据最后一根线来将三角形矫正）
    void correctTriangleItem(void);

    // 辅助函数（检测线如果碰撞就使其位移）
    void collidingMove(FlowLineItem *lineItem,QList<FlowLineItem *> lineItems_List);

    // 辅助函数（修正线元组：如果有不是正交线的情况）
    void fixLineItems(QList<FlowLineItem *> &m_LineItems_List);

    // 辅助函数（设置线元组里面每条线的鼠标样式，首尾两条线不能别选中和移动，要在归并后调用）
    void setLineItemCursorStyleAndFlags(void);

private:
    int m_LineItemID;

    // 源BlockItem
    DotItem *m_SouDotItem;

    // 目标BlockItem
    DotItem *m_DesDotItem;

    // 线元组链表
    QList<FlowLineItem *> m_LineItems_List;

    // 箭头三角形
    QGraphicsPolygonItem *m_TriangleItem;
};

#endif // LINEITEMCLASS_H
