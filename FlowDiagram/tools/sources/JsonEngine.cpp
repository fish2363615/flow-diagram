﻿#include "../../tools/headers/JsonEngine.h"

JsonEngine::JsonEngine(QString filePath)
{
    m_FileAllData = readFile(filePath);
}

// f辅助函数（解析JSON数据）
void JsonEngine::analysisJsonData(void)
{
    QJsonParseError jsonError;
    QJsonDocument jsonDocument;
    QJsonObject rootObj;

    // 用json格式读取QByteArray类型数据
    jsonDocument = QJsonDocument::fromJson(m_FileAllData,&jsonError);

    // 如果文件的格式有问题，就报错并退出
    if(jsonError.error != QJsonParseError::NoError)
    {
        qDebug()<<"json error:"<<jsonError.error;
        return;
    }

    // 获取根对象
    rootObj = jsonDocument.object();

    // 获取每一个键值对的键值
    QStringList keys = rootObj.keys();

    QJsonArray blockItems = rootObj["BlockItems"].toArray();

    for (int i = 0; i < blockItems.size(); ++i)
    {
        QJsonObject jsonTempObj =  blockItems.at(i).toObject();
        QJsonObject jsonTextObj = jsonTempObj["BlockItem_TextItem"].toObject();
        qDebug()<<"BlockItem:"<<jsonTextObj["Text"].toString();
    }

}

//
QJsonDocument JsonEngine::toJsonDocument(void)
{
    QJsonParseError jsonError;
    QJsonDocument jsonDoc = QJsonDocument::fromJson(m_FileAllData,&jsonError);

    return jsonDoc;
}

// 通过文件名将文件内容全部读取到内存中
QByteArray JsonEngine::readFile(QString file_Path)
{
    QFile file(file_Path);

    QByteArray fileAllData;


    if(!file.exists())
    {
        // 在此提示异常：文件不存在
        return fileAllData;
    }

    if(!file.open(QIODevice::ReadOnly))
    {
        // 在此体会是异常：文件未能成功打开
        return fileAllData;
    }

    fileAllData = file.readAll();
    file.close();

    return fileAllData;
}

